import React from 'react'
import classes from './Burger.css'
import BurgerIngrediant from './BurgerIngrediant/BurgerIngrediant'
const burger = (props) => {
    const transformedIngredients = Object.keys(props.ingredients)
    .map(igKey => {
        return [...Array(props.ingredients[igKey])].map((_,i) => <BurgerIngrediant key={igKey+i} type={igKey}/>)
    })
    return  (<div className={classes.Burger}>
        <BurgerIngrediant type="bread-top"></BurgerIngrediant>
        {transformedIngredients}
        <BurgerIngrediant type="bread-bottom"></BurgerIngrediant>
    </div>);
}

export default burger;